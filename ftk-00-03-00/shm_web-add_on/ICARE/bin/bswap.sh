#!/bin/sh

if [ $# -eq 0 ]; then
  _input=`cat`
else
  _input="$*"
fi

echo "${_input}" | awk '{ n=length($1)-1; for(i=n;i>=1;i=i-2) printf "%s",substr($1,i,2) } END { printf "\n" }'

exit 0
