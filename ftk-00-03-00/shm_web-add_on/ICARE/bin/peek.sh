#!/bin/sh

if [ $# -eq 1 ]; then
  input=`cat`
else
  input="$1"
fi

echo "$input" | cut -d ' ' -f $2
