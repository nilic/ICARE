#!/bin/sh

if [ $# -eq 0 ]; then
  input=`cat`
else
  input="$*"
fi

echo "$input" | grep 'Response data:' | cut -d ' ' -f 3-
