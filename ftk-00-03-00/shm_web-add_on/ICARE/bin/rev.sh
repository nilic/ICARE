#!/bin/sh

if [ $# -eq 0 ]; then
  input=`cat`
else
  input="$*"
fi

echo $input | awk '{ n=split($0, arr,"");  for(i=1; i<=n; i++) s=arr[i] s } END { print s }'

exit 0
