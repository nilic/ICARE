#!/bin/sh

logger -t ICARE_CGI "Installing CGI symbolic links."

if [ ! -h /usr/httpd/html/index.html ]; then
  mv /usr/httpd/html/index.html /usr/httpd/html/index.html.org
fi

echo "Create symbolic link /usr/httpd/html/index.html"
ln -fs /usr/httpd/html/ICARE/shmm500_index.html /usr/httpd/html/index.html

echo "Create symbolic link /usr/httpd/html/ICARE"
ln -fs /etc/home/root/ICARE /usr/httpd/html/ICARE

dst_dir="/usr/httpd/cgi-bin/shmm"
src_dir="/etc/home/root/ICARE/cgi-bin"
cd $src_dir
shs=`ls *.sh`
cgis=`ls *.cgi`
files="$shs $cgis"
for f in $files;
do
  echo "Create symbolic link $dst_dir/$f"
  ln -fs $src_dir/$f $dst_dir/$f
done

exit 0
