#!/bin/sh

# Sourcing common header and query parser.
HDR_TITLE="MCU Information"
. ./cmn_hdr_qry.sh

for m in $_mcus;
do
	CLI_ARGS="$_boardAddress 2e 1 2e a1 0 $m"
	. ./cmn_cli.sh

	# Getting and analyzing MCU identifier data.
	echo "<hr>"
	_mcuID_LSB=`echo ${CLI_RESULTS} | cut -d ' ' -f4`
	_mcuID_MSB=`echo ${CLI_RESULTS} | cut -d ' ' -f5`
	_mcuID=`echo "${_mcuID_LSB}${_mcuID_MSB}" | awk '{ n=length($1)-1; for(i=n;i>=1;i=i-2) printf "%s",substr($1,i,2) } END { printf "\n" }'`
	echo "<h4>MCU Type: "
	case $_mcuID in
		"193C")
			echo "IPMC"
			;;
		"101F")
			echo "IOIF"
			;;
		*)
			echo "Unknown"
			;;
	esac
	echo "</h4>"
	echo

	echo "<tt style=\"text-align:center; color:black\">"
	echo "MCU ID: ${_mcuID}<br/>"

	# Getting and analyzing PCB version number.
	_pcbMinorLSB=`echo ${CLI_RESULTS} | cut -d ' ' -f6 | awk '{ printf("%d", "0x"$1); }'`
	_pcbMinorMSB=`echo ${CLI_RESULTS} | cut -d ' ' -f7 | awk '{ printf("%d", "0x"$1); }'`
	_pcbMinor=`expr $_pcbMinorMSB \* 256 + $_pcbMinorLSB`
	_pcbMajorLSB=`echo ${CLI_RESULTS} | cut -d ' ' -f8 | awk '{ printf("%d", "0x"$1); }'`
	_pcbMajorMSB=`echo ${CLI_RESULTS} | cut -d ' ' -f9 | awk '{ printf("%d", "0x"$1); }'`
	_pcbMajor=`expr $_pcbMajorMSB \* 256 + $_pcbMajorLSB`
	echo "PCB Version: ${_pcbMajor}.${_pcbMinor} <br/>"
	echo

	# Getting and analyzing Serial Number.
	_sn1=`echo ${CLI_RESULTS} | cut -d ' ' -f10 | awk '{ printf("%d", "0x"$1); }'`
	_sn2=`echo ${CLI_RESULTS} | cut -d ' ' -f11 | awk '{ printf("%d", "0x"$1); }'`
	_sn3=`echo ${CLI_RESULTS} | cut -d ' ' -f12 | awk '{ printf("%d", "0x"$1); }'`
	_sn4=`echo ${CLI_RESULTS} | cut -d ' ' -f13 | awk '{ printf("%d", "0x"$1); }'`
	_sn=`expr $_sn1 + 256 \* $_sn2 + 256 \* 265 \* $_sn3 + 256 \* 256 \* 256 \* $_sn4`
	echo "Serial Number: ${_sn} <br/>"
	echo "</tt>"
	echo
done

# Sourcing common footer.
PREV_URL="info"
. ./cmn_ftr.sh
