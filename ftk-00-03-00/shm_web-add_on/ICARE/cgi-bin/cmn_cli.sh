CLI_RESULTS=""

__cmd="clia sendcmd $CLI_ARGS"
#echo "__cmd='$__cmd'.<br/>"
__results=`$__cmd`
#echo "__results:$__results<br/>"  | sed 's:$:<br/>:g'

__failedCode=`echo "$__results" | grep "Failed to send the command, status:" | cut -d ' ' -f 11`
#echo "__failedCode='$__failedCode'.<br/>"
if [ "$__failedCode" != "" ];
then
	echo "<br/><tt style=\"text-align:center; color:red\"><HR><P><H2> Failed to send the command to board (status $__failedCode) ! </H2></P><HR></tt>"
	echo "</body>"
	echo "</html>"
	logger -t ICARGE_CGI $__results
	exit 0
fi

__statusCode=`echo "$__results" | grep "Completion code:" | cut -d ' ' -f 3`
#echo "__statusCode='$__statusCode'.<br/>"
if [ "$__statusCode" != "0x0" ];
then
	echo "<br/><tt style=\"text-align:center; color:red\"><HR><P><H2> Could not perform command (status $__statusCode) ! </H2></P><HR></tt>"
	echo "</body>"
	echo "</html>"
	logger -t ICARGE_CGI $__results
	exit 0
fi

CLI_RESULTS=`echo "$__results" | grep 'Response data:' | cut -d ' ' -f 3-`
#echo "CLI_RESULTS='$CLI_RESULTS'.<br/>"
