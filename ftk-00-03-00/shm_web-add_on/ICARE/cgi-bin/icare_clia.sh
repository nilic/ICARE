CLI_RESULTS=""

__cmd="clia sendcmd $CLI_ARGS"
#echo "__cmd='$__cmd'.<br/>"
__results=`$__cmd | grep -v "Pigeon Point Shelf Manager Command Line Interpreter"`
#echo "__results:$__results<br/>"  | sed 's:$:<br/>:g'

if [ `echo "$__results" | grep -c "Completion code:"` -eq 1 ];
then
	__statusCode=`echo "$__results" | grep "Completion code:" | cut -d ' ' -f 3 | sed 's:0x::g'`
	#echo "__statusCode='$__statusCode'.<br/>"
	if [ $__statusCode -ne 0 ];
	then
		echo "<tt style=\"text-align:center; color:red\"><HR><P><H2> Could not perform command (status $__statusCode) ! </H2></P></tt>"
		# Sourcing common footer.
		. ./icare_footer.sh
		logger -t ICARE_CGI "[$__cmd] ($__statusCode) $__results"
		exit 0
	fi
else
	__errorMsg=`echo "$__results" | tr -d '\n'`
	echo "<tt style=\"text-align:center; color:red\"><HR><P><H2> $__errorMsg ! </H2></P></tt>"
	# Sourcing common footer.
	. ./icare_footer.sh
	logger -t ICARE_CGI "[$__cmd] ($__failedCode) $__results"
	exit 0
fi

CLI_RESULTS=`echo "$__results" | grep 'Response data:' | cut -d ' ' -f 3-`
#echo "CLI_RESULTS='$CLI_RESULTS'.<br/>"
