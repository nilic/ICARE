#!/bin/sh

PREV_URL="ip_config"

# Sourcing common header and query parser.
HDR_TITLE="IP Configuration"
. ./icare_header.sh

echo "<HR>"

# Geting the IP Configuration.
echo "<tt style=\"text-align:center; color:black\">"

CLI_ARGS="$_boardAddress 2e 7 2e a1 0"
. ./icare_clia.sh

_macAddress=`echo ${CLI_RESULTS} | cut -d ' ' -f4-9 | tr ' ' ':'`
echo "MAC Address: ${_macAddress}<br/>"

_ipv4Address=`echo ${CLI_RESULTS} | cut -d ' ' -f10-13 | awk '{ printf("%d.%d.%d.%d", "0x"$1, "0x"$2, "0x"$3, "0x"$4); }'`
echo "IPv4 Address: ${_ipv4Address}<br/>"

# If in verbose mode:
if [ ! -z $_verbosity ]; then
	_subnetMask=`echo ${CLI_RESULTS} | cut -d ' ' -f14-17 | awk '{ printf("%d.%d.%d.%d", "0x"$1, "0x"$2, "0x"$3, "0x"$4); }'`
	echo "Subnet Mask: ${_subnetMask}<br/>"

	_gateway=`echo ${CLI_RESULTS} | cut -d ' ' -f18-21 | awk '{ printf("%d.%d.%d.%d", "0x"$1, "0x"$2, "0x"$3, "0x"$4); }'`
	echo "Gateway: ${_gateway}<br/>"

	_linkStatus=`echo ${CLI_RESULTS} | cut -d ' ' -f22`
	if [ $_linkStatus -eq 1 ]; then
		_linkStatus="Up"
	else
		_linkStatus="Down"
	fi
	echo "Link Status: ${_linkStatus}<br/>"

	_duplexMode=`echo ${CLI_RESULTS} | cut -d ' ' -f23`
	if [ $_duplexMode -eq 1 ]; then
		_duplexMode="Full"
	else
		_duplexMode="Half"
	fi
	echo "Duplex Mode: ${_duplexMode}<br/>"

	_linkSpeed=`echo ${CLI_RESULTS} | cut -d ' ' -f24 | awk '{ printf("%d", "0x"$1); }'`
	echo "Link Speed: ${_linkSpeed} Mb/s<br/>"
fi

echo "</tt>"
echo

# Sourcing common footer.
. ./icare_footer.sh
