hex to dec:
echo <hex> | awk '{ printf("%d", "0x"$1); }'
hex to ascii:
clia sendcmd 92 2e 3 2e a1 0 0 | grep 'Response data:' | cut -d ' ' -f 3- | cut -d ' ' -f 6- | sed 's:[0-9A-F][0-9A-F]: 0x&:g' |  awk '{ for(i=1;i<=NF;i++) if ("$i" != "0x00") printf("%c", $i); else printf("\n"); }'
