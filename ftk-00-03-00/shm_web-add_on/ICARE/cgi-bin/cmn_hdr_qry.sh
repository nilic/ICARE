# Generating HTML static header.
echo "<meta http-equiv="Content-Type" content="text/html; charset=utf-8">"
#"Content-Type: text/html"
echo
echo "<html> <!-- Copyright (c) 2012-2017 LAPP/CNRS. All rights reserved. -->"
echo "<head><title>ICARE ${HDR_TITLE}</title></head>"
echo "<body>"
echo "<A HREF="http://lappwiki.in2p3.fr/twiki/bin/view/AtlasLapp/Informatique"><IMG height=67 width=96 align="left" alt="" hspace=0 src="/ICARE/icare.gif" border=0></A>"
echo "<H1 style='color:#B29978; font-weight:normal'>&nbsp; <b>I</b>ntelligent platform management <b>C</b>ontroller softw<b>ARE</b> <br> &nbsp; ${HDR_TITLE}</H1>"
echo

# Outputing current time (for debugging purpose).
#echo "<h1>Time</h1>"
#echo "<tt style=\"text-align:center; color:cyan\">"
#date
#echo "</tt>"
#echo

# Save POST variables (only first time this is called)
if [ -z "$QUERY_STRING_POST" -a "$REQUEST_METHOD" = "POST" -a ! -z "$CONTENT_LENGTH" ]; then
  QUERY_STRING_POST=`cat`
fi
# Getting and analyzing POST query parameters.
_req_type=`echo "${QUERY_STRING_POST}"|cut -d '&' -f1|cut -d '=' -f2`
_addr1_0=`echo "${QUERY_STRING_POST}"|cut -d '&' -f2|cut -d '=' -f2`
_id2_1=`echo "${QUERY_STRING_POST}"|cut -d '&' -f3|cut -d '=' -f2`
_mcu=`echo "${QUERY_STRING_POST}"|cut -d '&' -f4|cut -d '=' -f2`
_mcus="0 1"
if [ $_mcu -eq 1 ]; then
        _mcus="1"
fi
if [ $_mcu -eq 0 ]; then
        _mcus="0"
fi

_protocol=`echo "${QUERY_STRING_POST}"|cut -d '&' -f5|cut -d '=' -f2`

_verbosity=`echo "${QUERY_STRING_POST}"|cut -d '&' -f6|cut -d '=' -f2`
if [ ! -z $_verbosity ]; then
	echo "Verbose mode turned on<br/>"
fi
_boardAddress=""
if [ $_req_type -eq 0 ]; then
        _boardAddress=$_addr1_0
else
        _boardAddress="board $_id2_1"
fi
echo "<br/>"

# Outputing Query details (for debugging purpose).
#echo "<h1>Query String</h1>"
#echo "<tt style=\"text-align:center; color:green\">"
#echo "QUERY_STRING_POST=\"${QUERY_STRING_POST}\"<br><br>"
##echo "${QUERY_STRING_POST}" | sed 's:&:<br/>:g'
#echo "_req_type='$_req_type'.<br/>"
#echo "_addr1_0='$_addr1_0'.<br/>"
#echo "_id2_1='$_id2_1'.<br/>"
#echo "_mcu='$_mcu'.<br/>"
#echo "_protocol='$_protocol'.<br/>"
#echo "_verbosity='$_verbosity'.<br/>"
#echo "<br/>"
#echo "_mcus='$_mcus'.<br/>"
#echo "_boardAddress='$_boardAddress'.<br/>"
#echo "</tt>"
#echo

if [ $_req_type -eq 0 -a -z "${_addr1_0}" ] || [ $_req_type -ne 0 -a -z "${_id2_1}" ]; then
	echo "<br/><tt style=\"text-align:center; color:red\"><HR><P><H2> Please provide a valid board address ! </H2></P><HR></tt>"
	echo "</body>"
	echo "</html>"
	exit 0
fi

# Check board address validity ?
CLI_ARGS="$_boardAddress 2e 1 2e a1 0 0"
. ./cmn_cli.sh
#echo "CLI_RESULT='$CLI_RESULT'."
