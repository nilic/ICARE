
# Generating HTML static footer.
echo "<hr>"

echo "<P>
    Copyright &#169; 2012-2017 LAPP/CNRS. <BR>
    All rights reserved.
</P>"

echo "<ADDRESS> LAPP/CNRS (<A HREF="mailto:sylvain.lafrasse@lapp.in2p3.fr?subject=ICARE WEB Interface">sylvain.lafrasse@lapp.in2p3.fr</A>) </ADDRESS>
  <BR>
  <A HREF="../../ICARE/verbs/${PREV_URL}.html">Back to the previous page</A>"

echo "</body>"
echo "</html>"
exit 0
