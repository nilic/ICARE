#!/bin/sh

PREV_URL="inet_services"

# Sourcing common header and query parser.
HDR_TITLE="Internet Services"
. ./icare_header.sh

echo "<hr/>"

# Generating the Internet Services table row by row.
echo "<tt style=\"text-align:center; color:black\">"

# Getting the total number of Internet Services.
CLI_ARGS="$_boardAddress 2e 8 2e a1 0"
. ./icare_clia.sh
_isNb=`echo "${CLI_RESULTS}" | cut -d ' ' -f4 | awk '{ printf("%d", "0x"$1); }'`
echo "Total number of Internet Services: ${_isNb}<br/><br/>"

# Generating the Services table row by row.
_table="<table Border="3" Cellpadding="3" Cellspacing="3"> <tr> <td>Service Name</td> <td>Protocol</td> <td>Port Number</td> <td>Client IP</td> <td>Client Port Number</td> </tr>"
# Looping over services ID.
range=`awk 'BEGIN { for (i = 0; i < '"$_isNb"'; ++i) print i;}'`
for isID in $range
do
	# Get each service details.
	isHexID=`echo $isID | awk '{ printf("%x", $1); }'`
	CLI_ARGS="$_boardAddress 2e 9 2e a1 0 $isHexID"
	. ./icare_clia.sh

	_serviceProtocol=`echo "${CLI_RESULTS}" | cut -d ' ' -f 17`
	if [ $_protocol -eq 2 ] || [ $_protocol -eq $_serviceProtocol ]; then
		if [ $_serviceProtocol -eq 0 ]; then
			_serviceProtocol="TCP"
		else
			_serviceProtocol="UDP"
		fi

		_name=`echo "${CLI_RESULTS}" | cut -d ' ' -f 4-16 | sed 's:[0-9A-F][0-9A-F]:0x&:g' |  awk '{ for(i=1;i<=NF;i++) if ("$i" != "0x00") printf("%c", $i); else printf("\n"); }'`

		_portNumberLSB=`echo "${CLI_RESULTS}" | cut -d ' ' -f 18 | awk '{ printf("%d", "0x"$1); }'`
		_portNumberMSB=`echo "${CLI_RESULTS}" | cut -d ' ' -f 19 | awk '{ printf("%d", "0x"$1); }'`
		_portNumber=`expr $_portNumberMSB \* 256 + $_portNumberLSB`

		_clientIP=`echo "${CLI_RESULTS}" | cut -d ' ' -f 20-23 | awk '{ printf("%d.%d.%d.%d", "0x"$1, "0x"$2, "0x"$3, "0x"$4); }'`

		_clientPortNumberLSB=`echo "${CLI_RESULTS}" | cut -d ' ' -f 24 | awk '{ printf("%d", "0x"$1); }'`
		_clientPortNumberMSB=`echo "${CLI_RESULTS}" | cut -d ' ' -f 25 | awk '{ printf("%d", "0x"$1); }'`
		_clientPortNumber=`expr $_portNumberMSB \* 256 + $_portNumberLSB`

		_table="${_table} <tr> <td>${_name}</td> <td>${_serviceProtocol}</td> <td>${_portNumber}</td> <td>${_clientIP}</td> <td>${_clientPortNumber}</td> </tr>"
	fi
done
echo "${_table}</table>"
echo "</tt>"
echo

# Sourcing common footer.
. ./icare_footer.sh
