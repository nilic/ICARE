#!/bin/sh

PREV_URL="reset"

# Sourcing common header and query parser.
HDR_TITLE="IPMC Reset"
. ./icare_header.sh

echo "<hr>"
echo "<h4>MCU Type: "
case $_mcus in
	0)
		echo "IPMC"
		;;
	1)
		echo "IOIF"
		;;
	*)
		echo "Unknown"
		;;
esac
echo "</h4>"
echo

CLI_ARGS="$_boardAddress 2e a 2e a1 0 $m"
. ./icare_clia.sh
echo "<tt style=\"text-align:center; color:black\">"
echo "Reset command sent !<br/>"
echo "</tt>"

# Sourcing common footer.
. ./icare_footer.sh
