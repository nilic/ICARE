#!/bin/sh

PREV_URL="version"

# Sourcing PATH envar.
HDR_TITLE="Version Information"
. ./icare_header.sh

for m in $_mcus;
do
	# Retrieving Binary Name.
	echo "<hr>"
	echo "<h4>MCU Type: "
	case $m in
		0)
			echo "IPMC"
			;;
		1)
			echo "IOIF"
			;;
		*)
			echo "Unknown"
			;;
	esac
	echo "</h4>"
	echo

	echo "<tt style=\"text-align:center; color:black\">"
	CLI_ARGS="$_boardAddress 2e 6 2e a1 0 $m"
	. ./icare_clia.sh
	_binary_name=`echo "${CLI_RESULTS}" | cut -d ' ' -f 3- | sed 's:[0-9A-F][0-9A-F]:0x&:g' |  awk '{ for(i=1;i<=NF;i++) if ("$i" != "0x00") printf("%c", $i); else printf("\n"); }'`
	echo "Binary Name: ${_binary_name}<br/>"
	echo

	# Retrieving Release Version.
	CLI_ARGS="$_boardAddress 2e 2 2e a1 0 $m"
	. ./icare_clia.sh
	_major_version=`echo "${CLI_RESULTS}" | cut -d ' ' -f4 | awk '{ printf("%02d", "0x"$1); }'`
	_minor_version=`echo "${CLI_RESULTS}" | cut -d ' ' -f5 | awk '{ printf("%02d", "0x"$1); }'`
	_patch_version=`echo "${CLI_RESULTS}" | cut -d ' ' -f6 | awk '{ printf("%02d", "0x"$1); }'`
	echo "Release Version: ICARE-${_major_version}-${_minor_version}-${_patch_version}<br/>"
	echo

	# Getting and analyzing Compiler Version.
	_major_version=`echo "${CLI_RESULTS}" | cut -d ' ' -f7 | awk '{ printf("%d", "0x"$1); }'`                                                           
	_minor_version=`echo "${CLI_RESULTS}" | cut -d ' ' -f8 | awk '{ printf("%d", "0x"$1); }'`                                                           
	_patch_version=`echo "${CLI_RESULTS}" | cut -d ' ' -f9 | awk '{ printf("%d", "0x"$1); }'`                                                           
	_compiler_option=`echo "${CLI_RESULTS}" | cut -d ' ' -f10`
	case $_compiler_option in
		"00")
			_compiler_option="opt"
			;;
		"01")
			_compiler_option="dbg"
			;;
		*)
			_compiler_option="unknown"
			;;
	esac
	echo "Compiler: (GNU) gcc ${_major_version}.${_minor_version}.${_patch_version} (${_compiler_option=})<br/>"
	echo

	# Getting and analyzing Build details.
	_buildDay=`echo "${CLI_RESULTS}" | cut -d ' ' -f11 | awk '{ printf("%02d", "0x"$1); }'`
	_buildMonth=`echo "${CLI_RESULTS}" | cut -d ' ' -f12 | awk '{ printf("%02d", "0x"$1); }'`
	_buildYear=`echo "${CLI_RESULTS}" | cut -d ' ' -f13 | awk '{ printf("%02d", "0x"$1); }'`
	_buildHour=`echo "${CLI_RESULTS}" | cut -d ' ' -f14 | awk '{ printf("%02d", "0x"$1); }'`
	_buildMinute=`echo "${CLI_RESULTS}" | cut -d ' ' -f15 | awk '{ printf("%02d", "0x"$1); }'`
	_buildSecond=`echo "${CLI_RESULTS}" | cut -d ' ' -f16 | awk '{ printf("%02d", "0x"$1); }'`
	_buildDate=`date -d "${_buildMonth}${_buildDay}${_buildHour}${_buildMinute}20${_buildYear}.${_buildSecond}" +"%b %d %Y, %H:%M:%S"`
	echo "Build Date/time: ${_buildDate}<br/>"
	echo "</tt>"
	echo

	# If in verbose mode:
	if [ ! -z $_verbosity ]; then

		# Getting and analyzing Packages details
		echo "<tt style=\"text-align:center; color:black\">"

		# Cannot use icare_clia.sh because of "Shell nested to deeply !" bug (https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/5/html/5.7_Technical_Notes/busybox.html)

		# Getting the total number of packages.
		_result=`clia sendcmd $_boardAddress 2e 3 2e a1 0 0$m| grep 'Response data:' | cut -d ' ' -f 3-`
		#echo "${_result}" | sed 's:$:<br/>:g'
		_pkgNb=`echo "${_result}" | cut -d ' ' -f4 | awk '{ printf("%d", "0x"$1); }'`
		echo "Total number of packages: ${_pkgNb}<br/><br/>"

		# Generating the Packages table row by row.
		_table="<table Border="3" Cellpadding="3" Cellspacing="3"> <tr> <td>Package Name</td> <td>Version</td> </tr>"
		# Looping over packages ID.		
		range=`awk 'BEGIN { for (i = 0; i < '"$_pkgNb"'; ++i) print i;}'`
		for pkgID in $range
		do
			# Get eack package details.
			pkgHexID=`echo $pkgID | awk '{ printf("%x", $1); }'`
			_name=`clia sendcmd $_boardAddress 2e 4 2e a1 0 $m $pkgHexID | grep 'Response data:' | cut -d ' ' -f 6- | sed 's:[0-9A-F][0-9A-F]:0x&:g' |  awk '{ for(i=1;i<=NF;i++) if ("$i" != "0x00") printf("%c", $i); else printf("\n"); }'`
			_version=`clia sendcmd $_boardAddress 2e 5 2e a1 0 $m $pkgHexID | grep 'Response data:' | cut -d ' ' -f 6- | sed 's:[0-9A-F][0-9A-F]:0x&:g' |  awk '{ for(i=1;i<=NF;i++) if ("$i" != "0x00") printf("%c", $i); else printf("\n"); }'`
			_table="${_table} <tr> <td>$_name</td> <td>$_version</td> </tr>"
		done
		echo "${_table}</table>"
		echo "</tt>"
		echo
	fi
done

# Sourcing common footer.
. ./icare_footer.sh
